/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 * Übungsrobot von Andreas Hahn
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------
#include <string.h>
#include <stdlib.h>
#include "car.h"
#include <math.h>
#include <iostream>
#include <mysql++.h>
#include "timer.h"

//--------------------------------------------------------------------------
//                           Class Robot00
//--------------------------------------------------------------------------

class Robot44 : public Driver
{
public:
    // Konstruktor
    Robot44()
    {
        // Der Name des Robots
        m_sName = "Robot44";
        // Namen der Autoren
        m_sAuthor = "Andreas Hahn";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLACK;
        m_iTailColor = oYELLOW;
        m_sBitmapName2D = "car_yellow_black";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

   double kurven_gesw(double radius)    //Rückgabe der maximalen Kurvengeschwindigkeit
  {
    double rad;
    rad = radius<0 ? -radius : radius;  //Rechts oder Linkskurve
    return sqrt(rad * 32.2 * 0.98);      //Berechnung und Rückgabe der Kurvengeschwindigkeit
  }


  double spur_linkeseite(double links_abstand, double gesamtbreite)     //Funktion um auf der Linkenseite der Fahrbahn zu fahren
  {
      double faktor;                    //Faktor des Einlenkens
      double variabler_faktor_rechts;   //einlenkung nach rechts
      double variabler_faktor_links;    //einlenkung nach links

      faktor = 0.005;
      variabler_faktor_rechts = (-1/links_abstand) * faktor;  //einlenkung ist um so stärker je weiter der wagen links ist
      variabler_faktor_links = links_abstand*faktor;          //einlenkung ist um so stärker je weiter der wagen rechts ist



      double spur = 0.0;

      int i;
      if((links_abstand>gesamtbreite/4)) //lenkt nach links wenn der wagen weiter als gesamtbreite/4 nach rechts kommt
      {
        if(i==1)
        {
            spur = variabler_faktor_links;       //Lenkt pro durchlauf einmal ein und im nächsten wieder zurück dammit das auto wieder gerade fährt.
            i=0;
        }

        else
            {
                i=1;
                spur = +variabler_faktor_links;     //das else wird zuerst ausgeführt, da variable i unbekannt ist, bestimmt die verschiebungsrichtung
            }
        }


        int j;
        if((links_abstand<gesamtbreite/8))  //lenkt nach rechts wenn der wagen weiter als gesamtbreite/8 nach links kommt
        {
            if(j==1)
            {
                spur = +variabler_faktor_rechts;       //Lenkt pro durchlauf einmal ein und im nächsten wieder zurück dammit das auto wieder gerade fährt.
                j=0;
            }

            else
            {
                j=1;
                spur = variabler_faktor_rechts;     //wird zu erst ausgeführt und bestimmt die verschiebungsrichtung
            }
        }


        return spur;
  }


  double spur_rechteseite(double rechts_abstand, double gesamtbreite)   //Funktion um auf der Rechtenenseite der Fahrbahn zu fahren
  {
      double faktor;                    //Faktor des Einlenkens
      double variabler_faktor_rechts;   //einlenkung nach rechts
      double variabler_faktor_links;    //einlenkung nach links

      faktor = 0.005;
      variabler_faktor_links = (1/rechts_abstand) * faktor; //einlenkung ist um so stärker je weiter der wagen links ist
      variabler_faktor_rechts = -rechts_abstand * faktor;   //einlenkung ist um so stärker je weiter der wagen rechts ist

      int i;
      double spur = 0.0;
      if((rechts_abstand>gesamtbreite/4)) //lenkt nach rechts wenn der wagen weiter als gesamtbreite/4 nach links kommt
        {
            if(i==1)
            {
                spur = +variabler_faktor_links;       //Lenkt pro durchlauf einmal ein und im nächsten wieder zurück dammit das auto wieder gerade fährt.
                i=0;
            }

            else
            {
                i=1;
                spur = variabler_faktor_rechts;     //das else wird zuerst ausgeführt, da variable i unbekannt ist, bestimmt die verschiebungsrichtung
            }
        }


        int j;
        if((rechts_abstand<gesamtbreite/8))  //lenkt nach links wenn der wagen weiter als gesamtbreite/8 nach rechts kommt
        {

            if(j==1)
            {
                spur = variabler_faktor_rechts;       //Lenkt pro durchlauf einmal ein und im nächsten wieder zurück dammit das auto wieder gerade fährt.
                j=0;
            }

            else
            {
                j=1;
                spur = +variabler_faktor_links;     //wird zu erst ausgeführt und bestimmt die verschiebungsrichtung
            }
        }


        return spur;
  }


    int datenbank_eingaben(int id, const char* robot, /*double tmtime,*/ float starttime, float racetime, int laps, float velocity, float fuel, int damage, const char* userdata )
    {//Datenbankfunktion 1 nicht ganz vollständig


        mysqlpp::Connection conn( false );          //"root"


        if( !conn.connect( "rars", "localhost", "root", "123456") ) // Verbindung mit der Datenbank schlägt sie fehl wird das Programm abgebrochen
        {

            exit(0);
        }



        mysqlpp::Query query = conn.query();

        query << "insert into tmtrace values(" << id << ",'" << robot << "'," << /*tmtime << */"," << starttime << "," << racetime << ","
        << laps << "," << velocity << "," << fuel << "," << damage << ",'" << userdata << "');" << mysqlpp::quote;  //Eingabe des SQL Befehls


        mysqlpp::StoreQueryResult res = query.store();  //Übergabe des SQL Befehls

    }


    void mysql(situation& daten)
    {   //Datenbankfunktion 2 funktioniert vollständig

        mysqlpp::Connection conn( false );

        if( !conn.connect( "rars", "localhost", "root", "123456") )       // Verbindungsaufbau zur Datenbank //Passwort r2a0c1e4 Benutzername rars
        {
        return;
        }

        mysqlpp::Query query = conn.query("insert into tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES");
                                                                    // Auswahl der Datenbankfelder
        query << "("                                                // Zuweisung der entsprechenden Variablen
        << mysqlpp::quote << m_sName << ","
        << daten.start_time << ","
        << daten.time_count << ","
        << daten.laps_done << ","                                   // currentTrack ist eine globale Variable vom Typ Track* (Zeiger!). Dieser Zeiger wird vom
        << daten.v << ","                                           // race_manager verwendet und zeigt auf den aktuellen Track -> Klasse Track hat eine öffentliche
        << daten.fuel << ","                                        // Methode get_track_description auf die man zugreifen kann -> Rückgabewert ist ein Struct vom Typ
        << daten.damage  << ","                                     // track_desc der die Variable sName beinhaltet -> ist aktueller Streckenname!!!!
        << mysqlpp::quote << currentTrack->get_track_description().sName
        << ")";
        query.store();
    }


    double kurvenabbremsung(double geschwindigkeit, double next_radius)
    {
        double bremsdistantz;

        bremsdistantz = geschwindigkeit / (kurven_gesw(next_radius) * 1) * 150;    //desto größer die geschw. im vergleich zur kurvengeschw. ist desto größer wird der wert


        return bremsdistantz;
    }








//Haupt Implementierung
    con_vec drive(situation& s)
    {

        // hier wird die Logik des Robots implementiert
        con_vec result = CON_VEC_EMPTY;



        if( s.starting)
        {
            result.fuel_amount = MAX_FUEL;
        }

        if(stuck( s.backward, s.v, s.vn, s.to_lft, s.to_rgt, &result.alpha, &result.vc))
        {
            return result;
        }


        double speed = 0.0;
        double alpha = 0.0;
        double breite;
        double abstand = 20;

        breite = s.to_lft + s.to_rgt;   //Berechnung der Bahnbreite


        if(s.cur_rad == 0.0)
        {
            speed = s.v +50;

        }

        else
        {
            speed = kurven_gesw(s.cur_rad);
        }

    //500 - s.nex_rad
        if((s.nex_rad != 0.0) && (s.to_end < kurvenabbremsung(s.v , s.nex_rad))) //bremst vor der nächsten Kurve ab, desto früher je größer kurvenabbremsung ist.
        {
            speed = kurven_gesw(s.nex_rad);
        }







        double spurseite = 0.0;

        if(s.nex_rad > 0.0)     //ist die nächste Kruve eine Linkskurfe so hällt sich der Robot auf der rechten Seite und umgekehrt.
        {
            spurseite = spur_linkeseite(s.to_lft, breite);
        }

        else if(s.nex_rad < 0.0)
        {
            spurseite = spur_rechteseite(s.to_rgt, breite);
        }

        else
        {
            if(s.cur_rad < 0.0)
            {
                spurseite = spur_rechteseite(s.to_rgt, breite);
            }

            else
            {
                spurseite = spur_linkeseite(s.to_lft, breite);
            }
        }


        alpha = - sin(s.vn/s.v) * 1.5 + spurseite;  // bestimmt den winkel


        const char* eigene_eingabe;
        eigene_eingabe = "AAA";


        //datenbank_eingaben(s.my_ID, m_sName, /*s.time_count,*/ s.start_time, s.time_count, s.laps_done, s.v, s.fuel, s.damage, eigene_eingabe);
        mysql(s); //Datenbakfunktion 2





            result.vc = speed;
            result.alpha = alpha;
            result.request_pit = 0;


        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot44Instance()
{
    return new Robot44();
}




























//Datenbankanbindung ohne Funktion
/*
     mysqlpp::Connection conn( false );          //"root"


        if( !conn.connect( "rars", "localhost", "root", "123456") ) //user.c_str()
        {

            exit(0);
        }



        mysqlpp::Query query = conn.query();

        query << "insert into tmtrace values(" << s.my_ID << ",'" << m_sName << "'," << s.time_count << "," << s.start_time << "," << s.lap_time << ","
        << s.laps_done << "," << s.v << "," << s.fuel << "," << s.damage << ",'AAA');" << mysqlpp::quote;


        mysqlpp::StoreQueryResult res = query.store();
*/


