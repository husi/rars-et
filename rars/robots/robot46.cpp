/**
 * Robot46 im ET12-Software-Projekt
 *
 * @author    Sebastian Ziegler, Team 15
 * @version   V2
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <mysql++.h>
#include "car.h"

//--------------------------------------------------------------------------
//                           Class Robot46
//--------------------------------------------------------------------------

class Robot46 : public Driver
{

public:

    // Konstruktor
    Robot46(){

        // Der Name des Robots
        m_sName = "Robot46";
        // Namen der Autoren
        m_sAuthor = "Sebastian Ziegler, Team 15";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLACK;
        m_iTailColor = oLIGHTGRAY;
        m_sBitmapName2D = "car_black_black";
        // Fuer alle Gruppen gleich
        m_sModel3D = "futura";

    	// aktiviert den Tracemode (0 aus, 1 an)
		tracemode = 1;

        // robot-spezifische Attribute initialisieren
        regler_intDiff[0] = 0;
        regler_intDiff[1] = 0;
        iterationszaehler = 0;
        SQLconnected = 0;
        kollisionsvermeidung_timer = 0;
        kollisionsvermeidung_deadAheadSpur = 0;
        kollisionsvermeidung_letztesDeadAhead = 0;
    }


    con_vec drive(situation& s){

        con_vec result = CON_VEC_EMPTY;

        if(tracemode)
			SQLtrace(s);

        result.vc = geschwindigkeitsvorgabe(s) / MPH_FPS;
        result.alpha = lenkung(s);

        boxenstopp(s, result);

        iterationszaehler++;

        return result;
    }



private:

	bool tracemode;
	long iterationszaehler;
    double regler_intDiff[2];
    int kollisionsvermeidung_timer;
    double kollisionsvermeidung_deadAheadSpur;
    bool kollisionsvermeidung_letztesDeadAhead;
    mysqlpp::Connection conn;
    bool SQLconnected;
    std::string trackName;



    // lenkung
    // PID-Regler, Spurvorgabe und Kollisionsvermeidung fuer die Lenkung
	// in:     s: situation
	// return:    Lenkwinkel alpha
    double lenkung(situation& s){

        double aeussereSpur = (s.to_rgt + s.to_lft) * 0.5;  // max. Entfernung von der Fahrbahmitte
        double aktuelleSpur = s.to_lft - s.to_rgt;  // negativ -> links, positiv -> rechts, null -> Mitte

        double sollSpur = spurvorgabe(s, aeussereSpur);  // regulaere Spurvorgabe

        sollSpur = kollisionsvermeidung(s, sollSpur, aeussereSpur, aktuelleSpur);  // abgeaenderte Spurvorgabe bei Kollisionsgefahr

        //           SOLL        IST       Kp  Ki   Kd     Anpassung
        return PID(sollSpur, aktuelleSpur, 80, 1.4, 6000) / -100000;
    }


    // kollisionsvermeidung
    // Ändert die Soll-Spur bei Kollisionsgefahr
    // in:     s:            situation
    //         sollSpur:     vorgegebene Spur, die entweder duchgeschleift oder korrigiert wird
    //         aeussereSpur: max. Entfernung von der Fahrbahmitte
    //         aktuelleSpur: aktuelle Spur
	// return:               Sollspur
	double kollisionsvermeidung(situation& s, double sollSpur, double aeussereSpur, double aktuelleSpur){

        const int AUSWEICHZEIT = 100;  // minimale Ausweichzeit (Iterationen) nach Detektion eines "dead_ahead"
        const double MIN_ENTFERNUNG = 60;  // Entfernung zum Vordermann ab der ausgewichen wird

        if(s.dead_ahead && s.nearby[0].rel_y < MIN_ENTFERNUNG){

            kollisionsvermeidung_timer = AUSWEICHZEIT;

            if(s.dead_ahead != kollisionsvermeidung_letztesDeadAhead)  // Flankendetektor
                kollisionsvermeidung_deadAheadSpur = aktuelleSpur;
        }

        kollisionsvermeidung_letztesDeadAhead = s.dead_ahead;

        if(kollisionsvermeidung_timer > 0){  // Ausweichspur aktiv

            kollisionsvermeidung_timer--;

            if(kollisionsvermeidung_deadAheadSpur < aeussereSpur * 0.1)
                return aeussereSpur * 0.8;

            if(kollisionsvermeidung_deadAheadSpur > aeussereSpur * -0.1)
                return aeussereSpur * -0.8;
        }

        return sollSpur;
	}


	// spurvorgabe
	// Gibt die Fahrspur in Abhaengigkeit der Kurvenrichtungen vor.
	// in:     s: situation
	// return:    Spur (Mitte = 0)
    double spurvorgabe(situation& s, double aeussereSpur){

        const double SPURWECHSELDISTANZ = 600;  // Entfernung vor einer Kurve
        const double MAX_RADIUS = 800;  // Maximaler Radius, bei dem die Kurven geschnitten werden

        if(s.cur_rad != 0 || (s.nex_rad && s.to_end < SPURWECHSELDISTANZ)){  // Kurve in der Naehe oder Fahrzeug innerhalb einer Kurve

            if(fabs(s.cur_rad) > MAX_RADIUS || fabs(s.nex_rad) > MAX_RADIUS)
                return 0;
            if(s.cur_rad > 0 || s.nex_rad > 0)
                return -aeussereSpur;
            if(s.cur_rad < 0 || s.nex_rad < 0)
                return aeussereSpur;
        }

        return 0;  // keine Kurve in der Naehe, bzw. z.Z. keine Kurve
    }


    // geschwindigkeitsvorgabe
	// Gibt eine zur aktuellen Situation passende Sollgeschwindigkeit zurueck.
	// in:     s: situation
	// return:    Sollgeschwindigkeit in mph
    double geschwindigkeitsvorgabe(situation& s){

        double bremsentfernung = 0;
		const double BREMSENTFERNUNG_FAKTOR = 8500;  // Proportionalfaktor fuer Bremsentfernung

        if(s.nex_rad != 0)
            bremsentfernung = (1 / kurvengeschwindigkeit(s, 1)) * BREMSENTFERNUNG_FAKTOR;

        if(s.v > 100)
            bremsentfernung *= 2.8;  // wenn sehr schnell, dann frueher bremsen

        if(s.nex_rad != 0 && s.to_end <= bremsentfernung)  //  Kurve in der Naehe
            return kurvengeschwindigkeit(s, 1);

        if(s.cur_rad != 0 && s.to_end >= bremsentfernung)  // Kurve, keine weitere Kurve in der Naehe
            return kurvengeschwindigkeit(s, 0);

        return 200;  // Gerade, keine Kurve in der Naehe
    }


    // kurvengeschwindigkeit
    // Berechnet die Kurvengeschwindigkeit in abhaengigkit des Kurvenradius.
    // in:     s:    situation
	//         mode: 0 - Geschwindigkeit fuer aktuelle Kurve, 1 - fuer naechste Kurve
	// return:       Geschwindigkeit in mph
    double kurvengeschwindigkeit(situation& s, bool mode){

        const double FAKTOR = 17;  // Proportionalfaktor
        double vorgabe = 0;

        if(mode)
            vorgabe = sqrt(fabs(s.nex_rad) * FAKTOR);  // nach: Fz = (m*v^2)/r
        else
            vorgabe = sqrt(fabs(s.cur_rad) * FAKTOR);

        return fmax(vorgabe, 20);
    }


    // PID
    // PID-Regler mit Abtastzeitintervall = 1 und einfaches Anti-Windup
    // in:     soll:     Sollwert
    //         ist:      Istwert
    //         Kp:       proportionaler Faktor
    //         Ki:       integraler Faktor
    //         Kd:       differentieller Faktor
    // return:           Stellgroeße
    double PID(double soll, double ist, double Kp, double Ki, double Kd){

        // regler_intDiff[0]: Summe der Regelabweichungen (fuer integralen Anteil)
        // regler_intDiff[1]: alte Regelabweichung (fuer differenziellen Anteil)

        const double ANTI_WINDUP = 5000;  // Begrenzung des Integrators, notwendig wegen Boxenstopp
        double regelabweichung = soll - ist;

        regler_intDiff[0] += regelabweichung;

        regler_intDiff[0] = fmin(regler_intDiff[0], ANTI_WINDUP);
        regler_intDiff[0] = fmax(regler_intDiff[0], -ANTI_WINDUP);

        double stellgroesse = Kp * regelabweichung + Ki * regler_intDiff[0] + Kd * (regelabweichung - regler_intDiff[1]);

        regler_intDiff[1] = regelabweichung;

        return stellgroesse;
    }


    // boxenstopp
    // Initiiert bei Bedarf einen Boxenstopp
    // in: s:      situation
    //     result: control vector
    void boxenstopp(situation& s, con_vec& result){

        if(s.fuel < 10 || s.damage > 20000){

            result.request_pit = 1;
            result.repair_amount = s.damage * 0.8;
            result.fuel_amount = MAX_FUEL;
        }
    }


	// SQLtrace
	// Schreibt die aktuelle Situation bei jeder 20. Iteration in die SQL Datenbank.
	// in: s: situiation
    void SQLtrace(situation& s){

        const int EINTRAGSINTERVALL = 20;

        if(iterationszaehler == 0){
            SQLconnected = conn.connect("rars", "localhost", "rars", "r2a0c1e4");

            struct track_desc trackInfo = get_track_description(); // Rennstreckenname abfragen
            trackName = trackInfo.sName;
            trackName = trackName.substr(0, trackName.find(".trk"));  // .trk abschneiden
        }

        if((iterationszaehler % EINTRAGSINTERVALL) == 0 && SQLconnected){

            mysqlpp::Query query = conn.query();

            query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
            << mysqlpp::quote << m_sName   // Robotname
                              << ","
            << s.start_time   << ","       // Startzeit des Robots
            << s.time_count   << ","       // vergangene Zeit seit Start des Rennens
            << s.laps_done    << ","       // abgeschlossene Runden
            << s.v * MPH_FPS  << ","       // aktuelle Geschwindigkeit in MPH
            << s.fuel         << ","       // aktueller Tankfuellstand
            << s.damage       << ","       // aktueller Schaden
            << mysqlpp::quote << trackName // Rennstreckenname
            << ")";

            query.execute(); // fuehrt INSERT-Befehl aus
        }
    }

};



/**
 * Diese Methode darf nicht veraendert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot46Instance()
{
    return new Robot46();
}
