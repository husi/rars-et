/**
 * Übungs-Robot im Software-Projekt
 *
 * @author    Simon Schierle
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"

#include <math.h>
#include <stdlib.h>

using namespace std;

//--------------------------------------------------------------------------
//                           Class Robot03
//--------------------------------------------------------------------------

class Robot03 : public Driver
{
public:
    // Konstruktor
    Robot03()
    {
        // Der Name des Robots
        m_sName = "Robot03";
        // Namen der Autoren
        m_sAuthor = "Simon Schierle";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oGREEN;
        m_iTailColor = oGREEN;
        m_sBitmapName2D = "car_green_green";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

/*--------------------------------------------------

        FUNKTIONEN

------------------------------------------------*/

    /**
    * @brief    Bremswegberechnung
    * @param    v0  aktuelle Geschwindigkeit in fps
    * @param    v1  Zielgeschwindigkeit nach Bremsen in fps
    * @param    a   Beschleunigung in ft/s² (negativ!)
    * @return   Bremsweg in ft
    */
    double get_bremsweg (double v0, double v1, double a)
    {
        double dv; // Geschwindigkeitsdifferenz
        dv = v1 - v0;
        if (dv > 0.0)
        {
            return (0.0);
        }
        return (v0*dv/a-dv*dv/2*a);
        // return (v0 + .5 * dv) * dv / a;  // nach Tutorial
    }

    /**
    * @brief    Berechnet die maximale Kurvengeschwindigkeit
    * @param    radius  Der Kurvenradius
    * @return   Die maximale Kurvengeschwindigkeit
    */
    double get_max_kurv_geschw (double radius)
    {
        double reibung = 0.92;      //Reibungsfaktor für Kurven
        return sqrt(32.2 * radius * reibung);
        //return sqrt(1.2*g*radius); //von Dani
    }

    /**
    * @brief    Berechnet die Kurvengeschwindigkeit
    * @param    folge_rad    Radius der folgenden Kurve
    * @param    korrektur   Kurvenkorrektur
    * @return   Kurvengeschwindigkeit für nächste Kurve
    */
    double get_kurv_geschw (double folge_rad, double korrektur)
    {
        if (folge_rad > 0.0)
        {
            return get_max_kurv_geschw (korrektur + folge_rad);
        }

        else if (folge_rad < 0.0)
        {
            return get_max_kurv_geschw (korrektur - folge_rad);
        }

        else
        {
            return 250.0;
        }
    }

    /**
    * @brief    Berechnet Distanz zum Kurvenende
    * @param    radius      Kurvenradius
    * @param    bis_ende    Strecke bis Kurvenende
    * @param    korrektur   Kurvenkorrektur
    * @return   Distanz bis zum Kurvenende
    */
    double kurv_ende (double radius, double bis_ende, double korrektur)
    {
        if (radius > 0.0)
        {
            return bis_ende * (radius + korrektur);
        }
        else
        {
            return -bis_ende * (radius - korrektur);
        }
    }

    /**
    * @brief    Geschwindigkeit vor Kurve korrigieren
    * Beschleunigt oder bremst stark bei hoher Abweichung von
    * der Sollgeschwindigkeit und passt die Geschwindigkeit langsam
    * an bei geringer Abweichung vom Sollwert
    * @param    v_ist   Ist-Geschwindigkeit
    * @param    v_soll  Soll-Geschwindigkeit
    * @return
    */
    double geschw_korr (double v_ist, double v_soll)
    {
        // stark bremsen, wenn viel schneller als Soll
        if(v_ist > 1.02 * v_soll)
        {
            return 0.94 * v_ist;
        }

        // stark beschleunigen, wenn viel langsamer als Soll
        else if(v_ist < .98 * v_soll)
        {
            return 1.1 * v_soll;
        }

        // Geschwindigkeit langsam anpassen, wenn nahe am Soll
        else
        {
            return 0.5 * (v_ist + v_soll);
        }
    }


    /**
    * @brief
    * @param
    * @param
    * @return
    */

    con_vec drive(situation& s)
    {

//--------------------------------------------------
//
//          K O N S T A N T E N
//
//---------------------------------------------------

        const double KURVENKORREKTUR = 10.0;

        con_vec result = CON_VEC_EMPTY;

        if(s.starting)                  // Wird nur bei Rennstart aufgerufen
        {
            result.fuel_amount = 50;    // Tankfüllung
            //lane = lane0 = s.to_lft;    // better not to change lanes during "dragout"
        }

        // Service Routine: Schaden auf Null bei Start
        if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha,&result.vc))
        return result;




       // Initialisierung Variablen
        double geschw = 0.0;        // Geschwindigkeit
        double winkelkorr = 0.0;    // Korrekturfaktor für Winkel
        double folgegeschw = 0.0;   // nächste Geschwindigkeit
        double alpha = 0.0;         // Lenkwinkel
        double vc = 0.0;            //
        double kurvende = 0.0;      // Ende der Kurve
        double str_breite =  0.0;   // Streckenbreite
        double str_mitte = 0.0;     // Streckenmitte

        // Streckenbreite berechnen
        str_breite = s.to_lft + s.to_rgt;

        // Linkskurve
        if (s.cur_rad > 0.0)
        {
            str_mitte = KURVENKORREKTUR;
        }
        // Rechtskurve
        else
        {
            str_mitte = str_breite - KURVENKORREKTUR;
        }

        if(s.cur_rad == 0.0)
        {
            // Kurvengeschwindigkeit allgemein
            geschw = get_kurv_geschw (s.nex_rad, KURVENKORREKTUR);
        }

        else
        {
            if(s.nex_rad == 0.0)
            {
                folgegeschw = 250.0;
            }

            else
            {
                folgegeschw = get_max_kurv_geschw (fabs(s.nex_rad) + KURVENKORREKTUR);
            }

            geschw = get_max_kurv_geschw (fabs(s.cur_rad) + KURVENKORREKTUR);
            winkelkorr = (s.v * s.v / (geschw * geschw)) * atan(KURVENKORREKTUR / geschw);

            // Winkelkorrektur muss negativ sein fuer Linkskurve
            if(s.cur_rad < 0.0)
            {
                winkelkorr = -winkelkorr;
            }

        }

        alpha = 0.40* (s.to_lft - str_mitte)/str_breite - 1.10 * s.vn / s.v + winkelkorr; // Steuerfaktor: 0.50, Daempfungsfaktor: 1.10

        // Befindet man sich auf einer Geraden?
        if(s.cur_rad == 0.0)
        {

            // Wenn Bremsabstand noch ausreicht -> weiter beschleunigen
            if(s.to_end > get_bremsweg (s.v, geschw, -30.0))        // Bremsgeschwindigkeit auf Geraden: -30.0
            {
                vc = s.v + 50.0;
            }

            // Geschwindigkeit nahe Kurve anpassen
            else
            {

                // Geschwindigkeit nahe Kurve anpassen
                vc = geschw_korr (s.v, geschw);

                // Kurve nicht in Fahrbahnmitte nehmen sondern bezogen auf moeglichst kleinem Radius
                if(s.nex_rad > 0.0)
                {
                    str_mitte = KURVENKORREKTUR;
                }

                else
                {
                    str_mitte = str_breite - KURVENKORREKTUR;
                }
            }
        }

        // Verhalten innerhalb der Kurve
        else
        {
            vc = 0.5 * (s.v + geschw)/cos(alpha);

            // Abstand bis zum Kurvenende bestimmen
            kurvende = kurv_ende (s.to_end, s.cur_rad, KURVENKORREKTUR);

            // In Kurve abbremsen
            if(kurvende <= get_bremsweg (s.v, folgegeschw, -25.0))  // Bremsgeschwindigkeit in Kurve: -25.0
            {
                vc = s.v - 5.0;
            }
        }

        // Berechnete Geschwindigkeit und Winkel an Framework uebergeben.
        result.vc = vc;
        result.alpha = alpha;





        return result;
    }
};

/*
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot03Instance()
{
    return new Robot03();
}

