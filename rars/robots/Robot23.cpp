/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <cmath>
using namespace std;

//--------------------------------------------------------------------------
//                           Class Robot23
//--------------------------------------------------------------------------

class Robot23 : public Driver
{
private:

    // Konstanten für die Geschwindigkeitsberechnungen
    static const double GRAVITATION                    = 32.2;
    static const double HAFTREIBUNGSKOEFFIZIENT        = 0.99;
    static const double BREMSKOEFFIZIENT               = 0.25;
    static const double BESCHLEUNIGUNGSKOEFFIZIENT     = 1.50;
    static const double STRECKENKOEFFIZIENT            = 0.60;     // Streckenverlauf

    // Konstatnen für die Winkelberechnung
    static const double KURVENKOEFFIZIENT              = 0.004;    // ändert die Spur des Robots vor Kurven
    static const double DAEMPUFUNGSKOEFFIZIENT         = 0.015;    // verringert die Geschwindigkeit

    // Konstanten für den Boxenstop und den Start
    static const double START_TREIBSTOFF               = 20;
    static const double NACHTANKEN                     = 80;
    static const double SCHADEN_REPARIEREN             = 30000;
    static const double MIN_TREIBSTOFF                 = 10;
    static const double MAX_SCHADEN                    = 15000;
    static const double RUNDEN                         = 5;
    static const double MIL_TO_FT_COEFFICIENT          = 5280;
    static const double DURCHSCHNITTL_VERBRAUCH        = 0.73;

public:
    // Konstruktor
    Robot23()
    {
        // Der Name des Robots
        m_sName = "Robot23";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

    /**
     * Beschreibung: Erhöt die Geschwindigkeit
     * Parameter: in: Vektor situation s
     * Rückgabe: Erhöhte Geschwindigkeit
     */

    double beschleunigen( situation& s )
    {
        return s.v * BESCHLEUNIGUNGSKOEFFIZIENT;  // Robot beschleunigt
    }


    /**
     * Beschreibung: Bremst den Robot je nach Situation
     * Parameter: in: Vektor situation s
     * Rückgabe: Verringerte Geschwindigkeit
     */

    double bremsen( situation& s )
    {
        return berechnenNeachsteGeschwindigkeit( s ) + s.to_end * BREMSKOEFFIZIENT;   // Robot bremst
    }


    /**
     * Beschreibung: Berechnet die Geschwindigkeit in den Kurven
     * Parameter: in: Vektor situation s
     * Rückgabe: Maximale Kurvengeschwindigkeit
     */
    double berechnenKurvengeschwindigkeit( situation& s )
    {
        double offset = ( s.to_lft + s.to_rgt ) / 2;                            // Mittelspur
        double radius = abs( s.cur_rad ) + offset;                              // Radius der Kurve

            return sqrt( radius * GRAVITATION * HAFTREIBUNGSKOEFFIZIENT );      // maximale Kurvengeschwindigkeit
    }

    /**
     * Beschreibung: Berechnet die Geschwindigkeit für den nächsen Streckenabschnitt
     * Parameter: in: Vektor situation s
     * Rückgabe: Geschwindigkeit für den nächsen Streckenabschnitt
     */


    double berechnenNeachsteGeschwindigkeit( situation& s )
    {
        double offset = ( s.to_lft + s.to_rgt ) / 2;
        double radius = abs( s.nex_rad ) + offset;

        if( s.nex_rad != 0 )                                                    // vor einer Kurve
        {
            return sqrt( radius * GRAVITATION * HAFTREIBUNGSKOEFFIZIENT );      // maximale Kurvengeschwindigkeit
        }
        else                                                                    // vor einer geraden Strecke
        {
            return berechnenKurvengeschwindigkeit( s );                         // maximale Geschwindigkeit in der aktuellen Kurve
        }
    }

    /**
     * Beschreibung: Berechnet die Geschwindigkeit
     * Parameter: in: Vektor situation s
     * Rückgabe: Geschwindigkeit des Robots
     */

    double berechnenGeschwindigkeit( situation& s )
    {
        if( s.to_end < s.cur_len * STRECKENKOEFFIZIENT )                        // Robot kurz vor nächstem Streckenabschnitt
        {
            if( s.v > berechnenNeachsteGeschwindigkeit( s ) )                   // Robot zu schnell
            {
                return bremsen( s );                                            // Robot Bremst
            }
            else                                                                // Robot zu langsam
            {
                return beschleunigen( s );                                      // Robot Beschleunigt
            }
        }
        else                                                                    // Robot weit von nächstem Streckenabschnitt
        {
            if( s.cur_rad == 0 )                                                // Robot auf einer geraden Strecke
            {
                return beschleunigen( s );                                      // Robot beschleunigt
            }
            else                                                                // Robot in einer Kurve
            {
                return berechnenKurvengeschwindigkeit( s );                     // maximale Kurvengeschwindigkeit
            }
        }
    }


    /**
     * Beschreibung: Lenkt den Robot
     * Parameter: in: Vektor situation s
     * Rückgabe: Lenkwinkel
     */
    double lenken( situation& s )
    {
            double offset = ( s.to_lft + s.to_rgt ) / 2;
            return KURVENKOEFFIZIENT * ( s.to_lft - offset ) - DAEMPUFUNGSKOEFFIZIENT* s.vn;
    }


    /**
     * Beschreibung: Legt den Starttreibstoff fest
     * Parameter: in: Vektor situation s
     * Rückgabe: Starttreibstoff
     */
    double festlegenTreibstoff( situation& s )
    {
        if( s.starting )                                                                                  // Rennen beginnt
        {
            track_desc trackinformation = get_track_description();                                        // Kursinformationen
            double lengthOfTrack = trackinformation.length;                                               // Länge des Kurses
                return lengthOfTrack * RUNDEN  / ( MIL_TO_FT_COEFFICIENT * DURCHSCHNITTL_VERBRAUCH );     // Berechnet die Treibstoffmenge
        }
        else
        {
            return START_TREIBSTOFF;
        }
    }


    /**
     * Beschreibung: Legt den Zeitpunkt für einen Boxenstop fest
     * Parameter: in: Vektor situation s /out: Vektor result
     * Rückgabe: Keine
     */
    void anfordernBoxenstop( situation& s, con_vec* result )
    {

        if( s.fuel < MIN_TREIBSTOFF || s.damage > MAX_SCHADEN )
        {
            result->request_pit = 1;                               // Boxenstop angefordert
            result->fuel_amount = NACHTANKEN;                      // nachtanken
            result->repair_amount = SCHADEN_REPARIEREN;            // Schaden reparieren
        }
    }

    // Aufrufen der Funktionen
    con_vec drive( situation& s )
    {
        con_vec result = CON_VEC_EMPTY;

        result.vc = berechnenGeschwindigkeit( s );
        result.alpha = lenken( s );
        result.fuel_amount =festlegenTreibstoff( s );
        anfordernBoxenstop( s, &result );

        if( stuck( s.backward, s.v, s.vn, s.to_lft, s.to_rgt, &result.alpha, &result.vc ) )
        {
            return result;
        }
        return result;
    }
};

/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot23Instance()
{
    return new Robot23();
}
