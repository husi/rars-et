//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include "math.h"
#include <cmath>
#include <stdlib.h>

//--------------------------------------------------------------------------
//                           Class Robot43
//--------------------------------------------------------------------------

class Robot43 : public Driver
{
public:
    // Konstruktor
    Robot43()
    {
        // Der Name des Robots
        m_sName = "Robot43";
        // Namen des Autoren
        m_sAuthor = "Jan Petersen";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBROWN;
        m_iTailColor = oBLACK;
        m_sBitmapName2D = "car_black_brown";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }


/**********************************************************************************************************************************************/

//LOGIK DES ROBOT (PUBLIC)

    con_vec drive(situation& s){

            con_vec result = CON_VEC_EMPTY;

            if( s.starting )

            result.fuel_amount = Spritfunktion(s);      //Je nach Strecke und je nach Rundenzahl soll über den Tankinhalt am Anfang des Rennens entschieden werden

            result.alpha = Lenkwinkel_Berechnung(s);            //Lenkwinkelberechnung
            result.vc = geschwindigkeitsberechnung(s);               //Geschwindigkeitsberechnung

            return result;
    }

/**************************************************************************************************************/

//LOGIK DES ROBOT (PRIVATE)

    private:

//ASSISTENZFUNKTIONEN:

    double Spurhalteassistenzstaerke (situation& s) { //Funktion berechnet wie schnell das fahrzeug seine spur wieder erreicht, wenn es von der vorgegebenen Fahrlinie abweicht

        double spurassi = 0; //initialisierung der Variable spurassi

        spurassi = 0.012 - 0.00003*(s.v); //je schneller desto geringer die Intensität des Spurhalteassistenzen

        return spurassi;
    }; //Faktor zum berechnen des Lenkwinkels

        double Lenkungsdaempfung (situation& s) { //funktion berechnet wie stark die Dämpfung der Lekung sein darf

        double lenkdaempf = 0; //Initialisierung der Variable lenkdaempf

        lenkdaempf = 1 + 0.005*(s.v); //je schneller desto größer die Lenkdämpfung

        return lenkdaempf;
    };       //Faktor zum berechnen des Lenkwinkels


    double Bremswegberechnung(situation& s){                            //Berechnet Bremsweg in feet

        double Bremswegfaktor = 0.40;
        double Bremsweg = (((((s.v)*3.281)/10)*(((s.v)*3.281))/10)/3.281)*Bremswegfaktor;

        return Bremsweg;
    }
/**********************************************************************************************************************************************/

//FUNKTIONEN DIE WERTE FÜR result LIEFERN:

    double Spritfunktion(situation& s){             //hier wird später noch einiges reinprogrammiert
    return MAX_FUEL;
    }


   double Lenkwinkel_Berechnung(situation& s){

            double alpha = (Spurhalteassistenzstaerke(s)*(s.to_lft-fahrlinienrechner(s)))
                            -(Lenkungsdaempfung(s)*(sin(s.vn/s.v)));
            return alpha;
    }


    double fahrlinienrechner(situation& s){     //Diese Funktion soll eine gute Fahrspur liefern, (steckt noch etwas Arbeit drin)

            double fahrlinie; //Initialisierung der Variable fahrlinie (0.0 = ganz links 1.0 = ganz rechts --> 0.5 = Mitte der Fahrbahn
            double fahrbahnbreite = s.to_lft + s.to_rgt; //Breite der Fahrbahn ist leider Gottes unterschiedlich --> Berechnung erforderlich


            if((s.cur_rad==0 || s.cur_rad>=300 || s.cur_rad<=-300) && s.nex_rad>0 && s.to_end<(Bremswegberechnung(s)+360)) //Geradeaus (oder lange Kurve) in Linkskurve mit Bremsstrecke
            fahrlinie = 0.9*(fahrbahnbreite);


            if((s.cur_rad==0 || s.cur_rad>=300 || s.cur_rad<=-300) && s.nex_rad<0 && s.to_end<(Bremswegberechnung(s)+360)) //Geradeaus (oder lange Kurve) in Rechtskurve mit Bremsstrecke
            fahrlinie = 0.1*(fahrbahnbreite);

            if((s.cur_rad==0 || s.cur_rad>=300 || s.cur_rad<=-300) && s.nex_rad>0 && s.to_end<300) //Von Gerade in Linkskurve nach innen ziehen
            fahrlinie = 0.4*(fahrbahnbreite);

            if((s.cur_rad==0 || s.cur_rad>=300 || s.cur_rad<=-300) && s.nex_rad<0 && s.to_end<300) //Von Gerade in Rechtkurve nach innen ziehen
            fahrlinie = 0.6*(fahrbahnbreite);

            if(s.cur_rad<0) //In Rechtskurven innen fahren
            fahrlinie = 0.9*(fahrbahnbreite);

            if(s.cur_rad>0) //In Linkskurven innen fahren
            fahrlinie = 0.1*(fahrbahnbreite);


            if((s.cur_rad==0 || s.cur_rad>=300 || s.cur_rad<=-300) && s.to_end>=(Bremswegberechnung(s)+360)) //wenn Strecke gerade und Fahrzeug weit entfernt vom Bremspunkt fährt Robot 43 in Mitte
            fahrlinie = 0.5*(fahrbahnbreite);

            return fahrlinie;
    }

    double geschwindigkeitsberechnung(situation& s){        //Diese funktion soll in allen Situationen die eine angemessene Geschwindigkeit berechnen

            double geschwindigkeit = 0/MPH_FPS;     //Initialisierung der Variable geschwindigkeit in feet/s
            double seitliche_G_Kraft = 1.4;//Seilicher Grip, der in einer Kurve erwartet wird. Dient als faktor zur Geschwindigkeitsberechnung
            int scharfe_Kurve = 300;
            double wenig_G_Kraft = 1.0;

            if ((s.cur_rad==0) && s.to_end>=Bremswegberechnung(s))      //Wenns gerade aus geht geb ich gas
            geschwindigkeit = 1000/MPH_FPS;

            if ((s.cur_rad==0) && s.to_end<Bremswegberechnung(s))               //In Kurven muss langsamer gefahren werden, deshalb wird vor der Kurve abgebremst
            geschwindigkeit = sqrt( abs(s.nex_rad) * 32.81 * seitliche_G_Kraft); //relativ optimale Kurvengeschwindigkeit wird berechnet

            if ((s.cur_rad==0) && s.to_end<Bremswegberechnung(s))               //In Kurven muss langsamer gefahren werden, deshalb wird vor der Kurve abgebremst
            geschwindigkeit = sqrt( abs(s.nex_rad) * 32.81 * seitliche_G_Kraft); //relativ optimale Kurvengeschwindigkeit wird berechnet

            if (s.cur_rad<0 || s.cur_rad>0)                                    //Geschwindigkeit in der derzeitgen kurve wird berechnet
            geschwindigkeit = sqrt( abs(s.cur_rad) * 32.81 * seitliche_G_Kraft);
/*
            if (abs(s.nex_rad)<scharfe_Kurve && (s.cur_rad<0 || s.cur_rad>0) && (s.nex_rad<0 || s.cur_rad>0))
            geschwindigkeit = sqrt( abs(s.nex_rad) * 32.81 * wenig_G_Kraft);

            if (abs(s.nex_rad)>=scharfe_Kurve && (s.cur_rad<0 || s.cur_rad>0) && (s.nex_rad<0 || s.cur_rad>0))
            geschwindigkeit = sqrt( abs(s.cur_rad) * 32.81 * seitliche_G_Kraft);
*/
            return geschwindigkeit;
    }
};

Driver * getRobot43Instance() //Darf nicht geändert werden, holt Robot43!
{
    return new Robot43();
}
