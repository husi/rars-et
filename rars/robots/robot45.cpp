

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
#include <math.h>
#include <mysql++.h>

//--------------------------------------------------------------------------
//                           Class Robot45
//--------------------------------------------------------------------------


class Robot45 : public Driver
{
public:
    // Konstruktor
    Robot45()
    {
        // Der Name des Robots
        m_sName = "Robot45";
        // Namen der Autoren
        m_sAuthor = "Daniel Husfeldt";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oWHITE;
        m_iTailColor = oBLACK;
        m_sBitmapName2D = "car_black_black";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }


//**********************************************************************************************************************************************
//LOGIK DES ROBOT (PUBLIC)




    con_vec drive(situation& s){

        con_vec result = CON_VEC_EMPTY;

        if( s.starting )                            //
            result.fuel_amount = MAX_FUEL;              //Benzintank ist bei Rennbeginn befüllt.

        result.alpha = lenkwinkelberechnung(s);     //Lenken
        result.vc = geschwindigkeitsberechnung(s);  //Geschwindigkeit wählen

        datenbank_eingaben(s.my_ID, m_sName, s.time_count, s.start_time, s.time_count, s.laps_done, s.v, s.fuel, s.damage, "TEST_3");

        return result;
    }



//**********************************************************************************************************************************************
//**********************************************************************************************************************************************
//DATENBANK WIRD BESCHRIEBEN


   void datenbank_eingaben(int id, const char* robot, double tmtime, double starttime, float racetime, int laps, float velocity, float fuel, int damage, const char* userdata )
    {


        mysqlpp::Connection conn( false );          //"root"


        if( !conn.connect( "rars", "localhost", "root", "123456") ) // Verbindung mit der Datenbank schlägt sie fehl wird das Programm abgebrochen
            exit(0);


        mysqlpp::Query query = conn.query();

        query << "insert into tmtrace values(" << id << ",'" << robot << "'," << tmtime << "," << starttime << "," << racetime << ","
        << laps << "," << velocity << "," << fuel << "," << damage << ",'" << userdata << "');" << mysqlpp::quote;  //Eingabe des SQL Befehls


        mysqlpp::StoreQueryResult res = query.store();  //Übergabe des SQL Befehls

    }


//**********************************************************************************************************************************************
//**********************************************************************************************************************************************
//LOGIK DES ROBOT (PRIVATE)




    private:

    static const double FAHRSPURRUECKTREIBFAKTOR = 0.01275;  //Faktor, welcher für Lenkuungslogik benötigt wird
    static const double EINLENKVERSTAERKUNGSFAKTOR = 2.5;   //Faktor, welcher für Lenkuungslogik benötigt wird
    static const double GRUNDGESCHWINDIGKEIT = 100;         //Für Geschwindigkeitslogik: Grundsätzliche Geschwindigkeit auf einer Geraden, wenn Robot weit weg von Kurve
    static const double KURVENGESCHWINDIGKEIT = 90;         //Für Geschwindigkeitslogik

//**********************************************************************************************************************************************




   double lenkwinkelberechnung(situation& s){   //Berechnet, wie stark der Robot einlenken soll

/***********************************************************************************************************************************************
            //Erklärung der Lenkwinkelberechnungsfunktion:
            //Der 1. Teil    <FAHRSPURRÜCKTREIBFAKTOR*(s.to_lft-fahrspurberechnung(s)> kümmert sich darum, dass das Fahrzeug bei leichtem Abkommen
            //               von der gewünschten Fahrspur beim Befahren von Geraden wieder zur Fahrspur zurückgetrieben wird.
            //               Hierzu wird die Differenz zwischen dem linken Seitenabstand zur Fahrbahnbegrenzung von der gewünschten Fahrspur abgezogen
            //               und mit einem Faktor multipiliziert
            //Der 2. Teil    <sin(s.vn/s.v)*2.5> ergibt sich aus dem Schaubild "situatio.gif" aus dem Ordner ../rars/doc.
            //               Sie sorgt für eine stärkere Einlenkcharakteristik beim Einfahren in (engere) Kurven und kann mit
            //               EINLENKVERSTAERKUNGSFAKTOR gewichtet werden
*************************************************************************************************************************************************/

            double alpha = (FAHRSPURRUECKTREIBFAKTOR*(s.to_lft-fahrspurberechnung(s)))
                            -(EINLENKVERSTAERKUNGSFAKTOR*(sin(s.vn/s.v)));

            return alpha;
    }

//**********************************************************************************************************************************************




    double fahrspurberechnung(situation& s){    //Funktion, die dem Robot seine gewünschte Fahrspur auf der Strecke zurückgibt

            double fahrspur;                                //gewünschte Fahrspur des Robots
            double fahrbahnbreite = s.to_lft + s.to_rgt;    //Abstand des Fahrzeugs von den beiden Rändern = Fahrbahnbreite

/***********************************************************************************************************************************************
            // Erklärung der möglichen Streckensituationen:
            // 1) Fahrbahn geht geradeaus: s.cur_rad = 0
            // 2) Linkskurve: s.cur_rad wird positiv
            // 3) Rechtskurve: s.cur_rad wird negativ
*************************************************************************************************************************************************/

            if(s.cur_rad==0)                      //Geradeaus
            fahrspur = 0.5*(fahrbahnbreite);      //Mitte der Fahrbahn

            if(s.cur_rad>0)                       //Linkskurve
            fahrspur = 0.3*(fahrbahnbreite);      //Fahren zur Kurveninnenseite

            if(s.cur_rad<0)                       //Rechtskurve
            fahrspur = 0.7*(fahrbahnbreite);      //Fahren zur Kurveninnenseite

            return fahrspur;
    }

//**********************************************************************************************************************************************




    double geschwindigkeitsberechnung(situation& s){    //Funktion, die für die Geschwindigkeitswahl des Robots

            double wunschgeschwindigkeit=GRUNDGESCHWINDIGKEIT;   //Grundsätzliche Geschwindigkeit auf einer Geraden, wenn Robot weit weg von Kurve

            if((s.cur_rad==0)&&(s.to_end>400))                        //Wenn es geradeaus geht und der Streckenabschnitt noch lang ist:
            wunschgeschwindigkeit= 1000*GRUNDGESCHWINDIGKEIT;         //Vollgas (hoher Geschwindigkeitssollwert)
            else if(s.cur_rad != 0)                                   //Wenn eine Kurve kommt:
            wunschgeschwindigkeit= KURVENGESCHWINDIGKEIT;             //Langsam machen

            return wunschgeschwindigkeit;
    }

//**********************************************************************************************************************************************


};



/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */

Driver * getRobot45Instance()
{
    return new Robot45();
}
