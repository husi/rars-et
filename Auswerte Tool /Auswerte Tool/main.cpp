
#include <mysql++.h>
#include <iostream>
#include <stdio.h>
#include <string.h>

#include "Ausgaben.h"

using namespace std;

// enter
// Sorgt dafür dass immer nur der aktuelle Teil des Programms angezeigt wird und ältere Programmausgaben im Konsolefenster gellöscht werden.

void enter(){ // wartet auf eingabe der enter Taste.
    // Löscht etwaige Fehlerzustände die das Einlesen verhindern könnten.
    cin.clear();
    // Ignoriert soviele Zeichen im Puffer wie im Puffer vorhanden sind.
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    // Füge alle eingelesenen Zeichen in den Puffer bis ein Enter gedrückt wird.
    // cin.get() liefert dann das erste Zeichen aus dem Puffer zurück, welches aber ignoriert wird (interessiert ja nicht).
    cin.get();
}





int main(){

    Ausgaben aus;
    aus.max_id(); // Einspeichern der letzten ID in end_id.
    aus.max_id_einfuegen(); // Fügt die ermittelte max ID von max_id() von help_max_id in end_id.
    aus.start_id_letztefahrt(); // Einspeichern der letzten start ID in start_id.


    bool ende; // Variable um die Schleife zu beenden.

    ende = true;

    do{

        int wahl; // Variable für die Hauptmenue Auswahl.
        system("clear"); // löscht die gesamte display Anzeige.
        cout << "Hauptmenue\n" << endl // Anzeige Hauptmenue.
             << "1. Daten von allen Rennen anzeigen: " << endl
             << "2. Nummer ändern: " << endl
             << "3. IDs anzeigen: " << endl
             << "4. Renndaten Anzeigen und PNG erzeugen: " << endl
             << "0. Ende" << endl;

        cin >> wahl; // Hauptmenue Auswahl.


            switch(wahl){

            case 1:
            system("clear"); // sorgt dafür dass nur der ausgewählte Menuepunkt in der Console angezeigt wird.
            aus.renn_daten(); // Gibt die Daten aller gespeicherten Rennen auf dem Bildschirm aus (Datum, Streckenname, Start ID und End ID) auperdehm schreibt sie alle start und end IDs in zwei dateien.
            aus.max_id(); // max ID wird aus der Datenbank in die Variabel help max_id geschrieben und in die Datei end_id angehängt.
            aus.max_id_ausgabe(); // anhängen der max ID and die ausgabe.
            aus.fehler_auswertung();
            enter(); // wartet auf eingabe der enter Taste.
            break;

            case 2:
            system("clear");
            aus.nummer_eingabe();
            aus.fehler_auswertung();
            enter();
            break;

            case 3:
            system("clear");
            aus.daten_anzeige();
            aus.fehler_auswertung();
            enter();
            break;

            case 4:
            system("clear");

            aus.max_geschw();
            cout << endl;

            aus.avg_geschw();
            cout << endl;

            aus.max_schaden();
            cout << endl;

            aus.renndauer();
            cout << endl;

            aus.rundenzahl();
            cout << endl;

            aus.datei_messwerte_erzeugen();

            aus.png_erzeugen();
            aus.fehler_auswertung();
            enter();
            break;

            case 0:
            cout << "Beende Programm" << endl;
            ende = false; // Programm wird beendet schleifenbedingung wird nicht mehr erfüllt.
            break;

            default: // wird bei falscher Eingabe Aufgerufen.
            system("clear");
            cout << "Falsche eingabe" << endl;
            enter();
        }

    } while(ende);

    return 0;
}

