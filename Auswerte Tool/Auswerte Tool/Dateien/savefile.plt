set y2range [0:30000]
set y2tics 
set ytics nomirror
set terminal png
set output 'Diagramm.png'
set grid
set xlabel 'Zeit'
set ylabel 'Geschwindigkeit'
set y2label 'Schaden'
set nokey
plot 'Dateien/Messwerte.dat' using 1:2 axis x1y1 with lines, 'Dateien/Messwerte.dat' using 1:3 axis x1y2 with lines
