
#ifndef AUSGABEN_H_INCLUDED
#define AUSGABEN_H_INCLUDED
#include <iostream>
#include <string>


using namespace std;

class Ausgaben{

    private:

    string start_id; // Hier wird die Start ID gespeichert z.B. 63241.
    string end_id; // Hier wird die End ID gespeichert z.B.63242.
    string help_max_id; // Hier wird die Höchste ID gespeichert
    int max_geschwindigkeit; // Für die y-Achsen Skallierung.
    int max_zeit; // Für die x-Achsen Skallierung.


    public:

    Ausgaben();

    void max_id_ausgabe(); // gitb die Max_id aus

    void start_id_letztefahrt(); // sucht die startid der letzten Fahrt heraus und speichert sie in start_id (Konstruktor)

    void renn_daten(); // Gibt die Daten aller gespeicherten Rennen auf dem Bildschirm aus (Datum, Streckenname, Start ID und End ID) auperdehm schreibt sie alle start und end IDs in zwei dateien.

    void max_id(); // Gibt die letzte ID Adresse aus und hängt diese an die datei endID.dat an.

    void max_id_einfuegen(); // Schreibt die max ID aus help_max_id in end_id.

    void nummer_eingabe(); // Eingabe der Renn Nummer, Funktion liest den entsprechenden wert aus den Dateien in die beiden Variablen end_id und start_id.

    void daten_anzeige(); // Gibt die momentan gespeicherte Start und End ID aus.

    void max_geschw(); // Gibt die Maximalgeschwindigkeit aus.

    void avg_geschw(); // Bestimmt die Durchschnittsgeschwindigkeit.

    void max_schaden(); // Gibt die Maximalschadenspunktzahl aus.

    void renndauer(); // Bestimmt die Dauer des Rennens.

    void rundenzahl(); // Gibt die Anzahl der absolvierten Runden aus.

    void datei_messwerte_erzeugen(); //Datei mit den Geschwindigkeits, Schaden und Zeitwerten wird erzeugt.

    void png_erzeugen(); //PNG Datei wird erzeugt.

    //int test();


};


#endif // ANLAGE_H
