
#include <iostream>
#include <string>
#include <mysql++.h>
#include <fstream>
#include <sstream>

#include "Ausgaben.h"
#include "Konstanten.h"

#include <stdio.h>
#include <stdlib.h>


using namespace std;


// Konstruktor.
// Inizialisierung der Variablen. Dies=1 test

Ausgaben::Ausgaben(){

    start_id = "0"; // Konstruktor vorgabe.
    end_id = "0";
    help_max_id = "0";
    max_geschwindigkeit = 0;
    max_zeit = 0;
}



void Ausgaben::start_id_letztefahrt(){ //sucht die startid der letzten Fahrt heraus und speichert sie in start_id (Konstruktor)

    string help_start_id = "0";
    stringstream umwandlungsstream2;


    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT Max(id) as startid FROM tmtrace Where racetime = '0' AND robot = '" << ROBOT << "';" << mysqlpp::quote; // Gibt die größte startid aus
    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

      umwandlungsstream2 << res[i]["startid"]; //Ausgabe der letzten ID Nummer. umwandeln von int in stringstream "umwandlungsstream2"

      }

    umwandlungsstream2 >> help_start_id; //stringstream in string help_start_id speichern
    start_id = help_start_id; //in start_id (siehe Konstruktor) übertragen

    return;
}



// renn_daten.
// Gibt die Daten aller gespeicherten Rennen auf dem Bildschirm aus (Datum, Streckenname, Start ID und End ID) auperdehm schreibt sie alle start und end IDs in zwei dateien.

void Ausgaben::renn_daten(){

    ofstream datei2("Dateien/startID.dat"); // Datei für die startIDs
    ofstream datei3("Dateien/endID.dat"); // Datei für die endIDs

    if(!datei2)
    {
        cerr << "Datei wurde nicht gefunden" << endl;
        return;
    }

    if(!datei3)
    {
        cerr << "Datei wurde nicht gefunden" << endl;
        return;
    }

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT userdata AS strecke, id AS id, tmtime AS zeit FROM tmtrace Where racetime = '0' AND robot = '" << ROBOT <<"';" << mysqlpp::quote; // Gibt das Datum, die Strecke und die ID aller Datensätze aus bei der die racetime 0 ist und der robot mit ROBOT übereinstimmt.

    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    cout << "\t\tAlle gespeicherten Renndaten" << endl << endl << endl;
    cout << " Nr." << "\t" << setw(6) << "Datum" << setw(10) << "Zeit" << setw(20) << "Strecken Name" << "\t\tStart ID\tEnd ID" << endl << endl; // setw(15) reserviert einen Platz von 15 Feldern alle felder, die zu viel sind werden leer gelassen.

    bool j = false; // Variable die dafür sorgt, dass die erste end ID nicht ausgegeben wird.

    for(size_t i = 0; i < res.num_rows(); i++){

        if (j){ // nach dem ersten Durchlauf wird diese if Anweisung immer ausgeführt.

            cout << res[i]["id"] -1 << endl; // Ausgabe der end ID.
            datei3 << res[i]["id"]-1 << endl; // end ID wird in die Datei endID.dat geschrieben
        }

        j = true;
        cout << " " << i + 1 << "\t"; // Ausgabe der Nr..
        cout << setw(20) << res[i]["zeit"]; // Ausgabe des Datums.
        cout << setw(15) << res[i]["strecke"] << "\t\t"; // Ausgabe des Streckennamens.
        cout << res[i]["id"] << "\t\t"; // Ausgabe der start ID.
        datei2 << res[i]["id"] << endl; // start ID wird in die Datei startTD.dat geschrieben

    }

    datei2.close();
    datei3.close();

    return;
}



// max_id.
// Gibt die letzte ID Adresse aus und hängt diese an die datei endID.dat an.

void Ausgaben::max_id(){

    ofstream datei4("Dateien/endID.dat", ios::out|ios::app); // Max ID wird an die Datei endID.dat angehängt

    if(!datei4)
    {
        cerr << "Datei wurde nicht gefunden" << endl;
        return;
    }

    stringstream umwandlungsstream1;

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT  Max(id) AS id FROM tmtrace Where robot = '" << ROBOT << "';" << mysqlpp::quote; // Gibt die letzte ID Nummer aus wo robot mit ROBOT übereinstimmt.
    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }


    for(size_t i = 0; i < res.num_rows(); i++){

        umwandlungsstream1 << res[i]["id"]; // Ausgabe der letzten ID Nummer. umwandeln von int in stringstream "umwandlungsstream1".
        datei4 << res[i]["id"] << endl; // Max ID wird an die Datei endID.dat angehängt.

    }



    umwandlungsstream1 >> help_max_id; // stringstream in string help_max_id speichern damit eine ausgwählte max_id nicht bei mehrmaligem aufruf überschrieben wird.


    datei4.close();

    return;
}




// Schreibt die max ID aus help_max_id in end_id.
// die max ID wird von help_max_id in end_id geschrieben damit am Anfang letze ID im string end_id steht

void Ausgaben::max_id_einfuegen(){

    end_id = help_max_id;

    return;
}


// max_id_ausgabe.
// Ausgabe der help_max_id.

void Ausgaben::max_id_ausgabe(){

    cout << help_max_id; // Ausgabe der help_max_id.

    return;
}


// nummer_eingabe
// Eingabe der Renn Nummer, Funktion liest den entsprechenden wert aus den Dateien in die beiden Variablen end_id und start_id.

void Ausgaben::nummer_eingabe(){

    ifstream datei5;
    ifstream datei6;

    string puffer; // Puffer für die nichtbenötigten Zeilen idn den Dateien.

    int wahl;
    int anzahl;
    wahl = 0;
    anzahl = 0; // Anzahl der Einträge.

    cout << "Bitte Nr. waelen: ";
    cin >> wahl; // Auswahl der Renn Nummer.
    wahl--; // damit bei 1 angefangen werden kann zu zählen.

    datei5.open("Dateien/startID.dat", ios::in); // Um Datei startID.dat Zeilenweise auszulesen.
    datei6.open("Dateien/endID.dat", ios::in); // Um Datei endID.dat Zeilenweise auszulesen.

    for ( int i = 0; !datei5.eof(); i++)
    {

        if (i == wahl){ // Wahr bei eingegebener Zeile.

            getline(datei5, start_id); // einlesen des entsprechenden Wertes in start_id.

        }
        else {

             datei5.eof();
             getline(datei5,puffer); // alle anderen Zeilen werden in den Puffer geschreiben.
        }

        anzahl++;
    }

    for ( int j = 0; !datei6.eof(); j++){

        if (j == wahl){ // Wahr bei eingegebener Zeile

        getline(datei6, end_id); // einlesen des entsprechenden Wertes in end_id
        }

        else {

        datei6.eof();
        getline(datei6,puffer); // alle anderen Zeilen werden in den Puffer geschreiben
        }
    }

    if ((wahl > anzahl - 2) || (wahl <= -1)){ // Bei zu hoher ID oder zu niedriger ID

        start_id = "0";
        end_id = "0";

        cout << endl << "Falsche Eingabe bitte erneut eingeben." << endl;
    }

    else{

        cout << endl << "Rennen ausgewaelt!" << endl;
    }

    datei5.close();
    datei6.close();

    return;
}


// daten_anzeige
// Gibt die momentan gespeicherte Start und End ID aus.

void Ausgaben::daten_anzeige(){

    cout << "Start ID\tEndID" << endl << endl;
    cout << start_id << "\t\t" << end_id << endl; // Anzeige der momentanen Start und End ID.

    return;
}


// max_geschw.
// Gibt die Maximalgeschwindigkeit aus.

void Ausgaben::max_geschw(){


    stringstream help1; // Erzeugen des Hilf stringstreams help1 zur Auslesung von Daten aus der Datenbank in eine Variable

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT round(MAX(velocity),2) AS max_geschw FROM tmtrace WHERE id >= '"  // Dieser Befehl liefert die Maximalegeschwindigkeit zu den eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT <<"';" << mysqlpp::quote;


    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Maximal Geschwindigkeit: ";
        cout << res[i]["max_geschw"] << " mp/h" << endl; // Ausgabe der Maximalgeschwindigkeit.
        help1 << res[i]["max_geschw"]; // Die Maximale Geschwindigkeit wird in den Hilfs stringstream help1 geschrieben für spätere y-Achsen Skallierung.
    }

    help1 >> max_geschwindigkeit; // Die Maximale Geschwindigkeit wird in die Variable max_geschwindigkeit geschrieben.

    return;
}


// avg_geschw.
// Bestimmt die Durchschnittsgeschwindigkeit.

void Ausgaben::avg_geschw(){

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return;
    }

    mysqlpp::Query query = conn.query();

    query << "SELECT round(AVG(velocity),2) AS avg_geschw FROM tmtrace WHERE id >= '" // Dieser Befehl liefert die Durchschnittlichegeschwindigkeit im Bereich der beiden IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;


    mysqlpp::StoreQueryResult res = query.store();

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Durchschnitts Geschwindigkeit: ";
        cout << res[i]["avg_geschw"] << " mp/h" << endl; // Ausgabe der Durchschnittsgeschwindigkeit.
    }

    return;
}


// max_schaden.
// Gibt die Maximalschadenspunktzahl aus.

void Ausgaben::max_schaden()
{
    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return;
    }

    mysqlpp::Query query = conn.query();

    query << "SELECT round(MAX(damage),2) AS schaden FROM tmtrace WHERE id >= '" // Dieser Befehl liefert den Maximalenschaden im Bereich der eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;

    mysqlpp::StoreQueryResult res = query.store();

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Maximaler Schaden: ";
        cout << res[i]["schaden"] << endl; // Ausgabe des Maximalen Schadens.
    }

    return;
}


// renndauer.
// Bestimmt die Dauer des Rennens.

void Ausgaben::renndauer()
{

    stringstream help2; // Erzeugen des Hilf stringstreams help2 zur Auslesung von Daten aus der Datenbank in eine Variable

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return;
    }

    mysqlpp::Query query = conn.query();

    query << "select round(Max(racetime),2) - round(MAX(starttime),2) AS renndauer from tmtrace WHERE id >= '" // Dieser Befehl liefert die Dauer des Rennens im Bereich der eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;



    mysqlpp::StoreQueryResult res = query.store();


    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Renndauer: ";
        cout << res[i]["renndauer"] << " s" << endl; // Ausgabe der Renndauer.
        help2 << res[i]["renndauer"]; // Die Maximale Zeit wird in den Hilfs stringstream help2 geschrieben für spätere x-Achsen Skallierung.
    }

    help2 >> max_zeit; // Die Maximale Zeit wird in die Variable max_zeit geschrieben.

    return;
}


// rundenzahl.
// Gibt die Anzahl der absolvierten Runden aus.

void Ausgaben::rundenzahl(){

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return;
    }

    mysqlpp::Query query = conn.query();

    query << "select Max(laps) AS rundenzahl from tmtrace WHERE id >= '" // Dieser Befehl liefert die Anzahl der Gefahrenen Runden im Bereich der eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;

    mysqlpp::StoreQueryResult res = query.store();


    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Gefahrene Runden: ";
        cout << res[i]["rundenzahl"] << endl; // Ausgabe der Rundenzahl.
    }

    return;
}


// datei_messwerte_erzeugen
// erzeugt eine Datei mit den Messwerten, Zeit, Geschwindigkeit und Schaden.

void Ausgaben::datei_messwerte_erzeugen(){

    ofstream datei1("Dateien/Messwerte.dat");

    if(!datei1)
    {
        cerr << "Datei wurde nicht gefunden" << endl;
        return;
    }

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT velocity AS geschw, damage AS schaden, racetime AS zeit FROM tmtrace WHERE id >= '"  // Dieser Befehl liefert die Maximalegeschwindigkeit und den Schaden zu den eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT <<"';" << mysqlpp::quote;


    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        datei1 << res[i]["zeit"] << "\t\t" << res[i]["geschw"] << "\t\t" << res[i]["schaden"] << endl; // Ausgabe der Maximalgeschwindigkeit und des Schadens.
    }

    datei1.close();

    return;

}


// pmg_erzeugen
// PNG Datei wird erzeugt.

void Ausgaben::png_erzeugen(){


    FILE *eingabe;
    eingabe = popen("gnuplot","w"); // Gnuplot wird geöffnet, um Befehle einzugeben.
    fprintf(eingabe, "set terminal png\n");                                         // Stellt das Terminal auf PNG
    fprintf(eingabe, "set xrange[0:%i]\n" , (max_zeit) + 10); // Skallierung der x-Achse.

    fprintf(eingabe, "set yrange[0:%i]\n", (max_geschwindigkeit) + 10); // Skallirung der y-Achse.
    fprintf(eingabe, "load 'Dateien/savefile.plt'\n"); // Laden der Diagramm Einstellungen.
    fprintf(eingabe, "set output\n"); // Datei wird Freigegeben.
    fclose(eingabe);

    cout << "PNG Datei wurde erzeugt" << endl;


    return;

}


/*int Ausgaben::test(){

    ifstream datei2("Maximale Geschwindigkeit.dat");

    if(!datei2)
    {
        cerr << "Datei wurde nicht gefunden" << endl;
        return -1;
    }

     datei2 >> max_geschwindigkeit;
     cout << max_geschwindigkeit;


    datei2.close();

}*/
