/**
 * Robot46 im ET12-Software-Projekt
 *
 * @author    Sebastian Ziegler
 * @version   Team 15 Robot V1
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <mysql++.h>
#include "car.h"

//--------------------------------------------------------------------------
//                           Class Robot46
//--------------------------------------------------------------------------

class Robot46 : public Driver
{

public:

    // Konstruktor
    Robot46(){

        // Der Name des Robots
        m_sName = "Robot46";
        // Namen der Autoren
        m_sAuthor = "Sebastian Ziegler";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLACK;
        m_iTailColor = oBLACK;
        m_sBitmapName2D = "car_black_black";
        // FÃ¼r alle Gruppen gleich
        m_sModel3D = "futura";

    	// aktiviert den Tracemode (0 aus, 1 an)
		tracemode = 1;

        // robot-spezifische Attribute initialisieren
        lenkung_reglerattribute[0] = 0;
        lenkung_reglerattribute[1] = 0;
    }


    con_vec drive(situation& s){

        con_vec result = CON_VEC_EMPTY;

/*        // service routine in the host software to handle getting unstuck from from crashes and pileups:
        if(stuck(s.backward, s.v,s.vn, s.to_lft,s.to_rgt, &result.alpha, &result.vc))
            return result;
*/

        if(tracemode)
			SQLtrace(s);

        result.vc = geschwindigkeitsvorgabe(s) / MPH_FPS;

        result.alpha = PID(spurvorgabe(s), s.to_rgt - s.to_lft, 80, 0.7, 5000, lenkung_reglerattribute);  // Lenkungsregelung
        result.alpha /= 100000;  // Anpassung um Kp, Ki und Kd auf handlichere Werte zu bringen

        return result;
    }



private:

	bool tracemode;
    double lenkung_reglerattribute[2];

    // PID
    // Standard PID-Regler mit Abtastzeitintervall = 1
    // in:     soll:     Sollwert
    //         ist:      Istwert
    //         Kp:       proportionaler Faktor
    //         Ki:       integraler Faktor
    //         Kd:       differentieller Faktor
    // inout:  attribut: Array mit zwei Elementen zur Zwischenspeicherung
    // return:           StellgrÃ¶ÃŸe
    double PID(double soll, double ist, double Kp, double Ki, double Kd, double* reglerdaten){

        // reglerdaten[0]: Summe der Regelabweichungen (fÃ¼r integralen Anteil)
        // reglerdaten[1]: alte Regelabweichung (fÃ¼r differenziellen Anteil)

        double regelabweichung = soll - ist;
        reglerdaten[0] += regelabweichung;
        double stellgroesse = Kp * regelabweichung + Ki * reglerdaten[0] + Kd * (regelabweichung - reglerdaten[1]);
        reglerdaten[1] = regelabweichung;
        return stellgroesse;
    }


    // geschwindigkeitsvorgabe
	// Gibt eine zur aktuellen Situation passende Sollgeschwindigkeit zurÃ¼ck.
	// in:     s: situation
	// return:    Sollgeschwindigkeit in mph
    double geschwindigkeitsvorgabe(situation& s){

	//	m_iTailColor = oLIGHTGRAY;  // fÃ¼r Debugging
    //	return 40;

        double bremsentfernung = 0;
		const double BREMSENTFERNUNG_FAKTOR = 8500;  // Proportionalfaktor fÃ¼r Bremsentfernung

        if(s.nex_rad != 0)
            bremsentfernung = (1 / kurvengeschwindigkeit(s, 1)) * BREMSENTFERNUNG_FAKTOR;

        if(s.v > 100)
            bremsentfernung *= 2.8;  // wenn sehr schnell, dann frÃ¼her bremsen

        if(s.nex_rad != 0 && s.to_end <= bremsentfernung)  //  Kurve in der NÃ¤he
            return kurvengeschwindigkeit(s, 1);

        if(s.cur_rad != 0 && s.to_end >= bremsentfernung)  // Kurve, keine weitere Kurve in der NÃ¤he
            return kurvengeschwindigkeit(s, 0);

    //	m_iTailColor = oRED;  // fÃ¼r Debugging

        return 200;  // Gerade, keine Kurve in der NÃ¤he
    }


    // kurvengeschwindigkeit
    // Berechnet die Kurvengeschwindigkeit in abhÃ¤ngigkit des Kurvenradius.
    // in:     s:    situation
	//         mode: 0 - Geschwindigkeit fÃ¼r aktuelle Kurve, 1 - fÃ¼r nÃ¤chste Kurve
	// return:       Geschwindigkeit in mph
    double kurvengeschwindigkeit(situation& s, bool mode){

        const double FAKTOR = 17;  // Proportionalfaktor

        if(mode)
            return sqrt(fabs(s.nex_rad) * FAKTOR);  // nach: Fz = (m*v^2)/r
        else
            return sqrt(fabs(s.cur_rad) * FAKTOR);
    }


	// spurvorgabe
	// Gibt die Fahrspur in AbhÃ¤ngigkeit der Kurvenrichtungen vor.
	// in:     s: situation
	// return:    Spur (Mitte = 0)
    double spurvorgabe(situation& s){

        const double SPURWECHSELDISTANZ = 600;  // Entfernung vor einer Kurve
        const double MAX_RADIUS = 800;  // Maximaler Radius, bei dem die Kurven geschnitten werden
        double offset = (s.to_rgt + s.to_lft) * 0.5;  // Entfernung von der Fahrbahmitte

        if(s.cur_rad != 0 || (s.nex_rad && s.to_end < SPURWECHSELDISTANZ)){  // Kurve in der NÃ¤he oder Fahrzeug innerhalb einer Kurve

            if(fabs(s.cur_rad) > MAX_RADIUS || fabs(s.nex_rad) > MAX_RADIUS)
                return 0;
            if(s.cur_rad > 0 || s.nex_rad > 0)
                return offset;
            if(s.cur_rad < 0 || s.nex_rad < 0)
                return -offset;
        }

        return 0;  // keine Kurve in der NÃ¤he, bzw. z.Z. keine Kurve
    }


	// SQLtrace
	// Schreibt die aktuelle Situation in die SQL Datenbank.
	// in: s: situiation
    void SQLtrace(situation& s){

        struct track_desc trackInfo = get_track_description(); // Rennstreckenname abfragen
        std::string trackName = trackInfo.sName;
        trackName = trackName.substr(0, trackName.find(".trk"));  // .trk abschneiden

        mysqlpp::Connection conn(false);

        if(!conn.connect( "rars", "localhost", "rars", "r2a0c1e4"))
            return;  // abbrechen, wenn keine Verbindung mÃ¶glich

        mysqlpp::Query query = conn.query();

        query << "INSERT INTO tmtrace (robot, starttime, racetime, laps, velocity, fuel, damage, userdata) VALUES ("
        << mysqlpp::quote << m_sName   // Robotname
                          << ","
        << s.start_time   << ","       // Startzeit des Robots
        << s.time_count   << ","       // vergangene Zeit seit Start des Rennens
        << s.laps_done    << ","       // abgeschlossene Runden
        << s.v * MPH_FPS  << ","       // aktuelle Geschwindigkeit in MPH
        << s.fuel         << ","       // aktueller TankfÃ¼llstand
        << s.damage       << ","       // aktueller Schaden
        << mysqlpp::quote << trackName // Rennstreckenname
        << ")";

        query.execute(); // fÃ¼hrt INSERT-Befehl aus

        return;
    }

};

/**
 * Diese Methode darf nicht verÃ¤ndert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot46Instance()
{
    return new Robot46();
}