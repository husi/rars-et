
#include <iostream>
#include <string>
#include <mysql++.h>

#include "Ausgaben.h"
#include "Konstanten.h"

using namespace std;


// Konstruktor.
// Inizialisierung der Variablen.

Ausgaben::Ausgaben(){

    start_id = "0"; // Konstruktor vorgabe.
    end_id = "0";
}


// renn_daten.
// Gibt die Daten aller gespeicherten Rennen auf dem Bildschirm aus (Datum, Streckenname, Start ID und End ID).
// return: return -1 Wenn keine Datenbankverbindung aufgebaut werden konnte, return -1 Wenn keine Daten gefunden wurden.

int Ausgaben::renn_daten(){

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return -1;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT userdata AS strecke, id AS id, tmtime AS zeit FROM tmtrace Where racetime = '0' AND robot = '" << ROBOT <<"';" << mysqlpp::quote; // Gibt das Datum, die Strecke und die ID aller Datensätze aus bei der die racetime 0 ist und der robot mit ROBOT übereinstimmt.

    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return -1;
    }

    cout << "\t\tAlle gespeicherten Renndaten" << endl << endl << endl;
    cout << setw(6) << "Datum" << setw(10) << "Zeit" << setw(20) << "Strecken Name" << "\t\tStart ID\tEnd ID" << endl << endl; // setw(15) reserviert einen Platz von 15 Feldern alle felder, die zu viel sind werden leer gelassen.

    bool j = false; // Variable die dafür sorgt, dass die erste end ID nicht ausgegeben wird.

    for(size_t i = 0; i < res.num_rows(); i++){

        if (j){ // nach dem ersten Durchlauf wird diese if Anweisung immer ausgeführt.

            cout << res[i]["id"] -1 << endl; // Ausgabe der end ID.
        }

        j = true;
        cout << setw(20) << res[i]["zeit"]; // Ausgabe des Datums.
        cout << setw(15) << res[i]["strecke"] << "\t\t"; // Ausgabe des Streckennamens.
        cout << res[i]["id"] << "\t\t"; // Ausgabe der start ID.

    }
}


// max_id.
// Gibt die letzte ID Adresse aus.
// return: return -1 Wenn keine Datenbankverbindung aufgebaut werden konnte, return -1 Wenn keine Daten gefunden wurden.

int Ausgaben::max_id(){

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return -1;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT  Max(id) AS id FROM tmtrace Where robot = '" << ROBOT << "';" << mysqlpp::quote; // Gibt die letzte ID Nummer aus wo robot mit ROBOT übereinstimmt.
    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return -1;
    }


    for(size_t i = 0; i < res.num_rows(); i++){

        cout << res[i]["id"] << endl; // Ausgabe der letzten ID Nummer.

    }
}


// daten_eingabe
// Eingabe der Start und End ID.

void Ausgaben::daten_eingabe(){

    cout << "Bitte geben Sie Start und End ID des gesuchten Rennens ein!"
         << endl << "Start ID: " << endl;
    cin >> start_id; // Start ID Eingabe in einen String.
    cout << "End ID: " << endl;
    cin >> end_id; // End ID Eingabe in einen String.

}


// daten_anzeige
// Gibt die momentan gespeicherte Start und End ID aus.

void Ausgaben::daten_anzeige(){

    cout << "Start ID\tEndID" << endl << endl;
    cout << start_id << "\t\t" << end_id << endl; // Anzeige der momentanen Start und End ID.
}


// max_geschw.
// Gibt die Maximalgeschwindigkeit aus.
// return: return -1 Wenn keine Datenbankverbindung aufgebaut werden konnte, return -1 Wenn keine Daten gefunden wurden.

int Ausgaben::max_geschw(){

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){ // Verbindung zur Datenbank wird aufgebaut.

        cerr << "Connection failed" << endl; // Im falle des Fehlschlagens wird eine Fehlermeldung gegeben un man muss zurück ins Hauptmenue um das Programm zu beenden.
        return -1;
    }

    mysqlpp::Query query = conn.query();
    query << "SELECT MAX(velocity) AS max_geschw FROM tmtrace WHERE id >= '"  // Dieser Befehl liefert die Maximalegeschwindigkeit zu den eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT <<"';" << mysqlpp::quote;


    mysqlpp::StoreQueryResult res = query.store(); // Befehl wird an die Datenbank übergeben.

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return -1;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Maximal Geschwindigkeit: ";
        cout << res[i]["max_geschw"] << endl; // Ausgabe der Maximalgeschwindigkeit.
    }
}


// avg_geschw.
// Bestimmt die Durchschnittsgeschwindigkeit.
// return: return -1 Wenn keine Datenbankverbindung aufgebaut werden konnte, return -1 Wenn keine Daten gefunden wurden.

int Ausgaben::avg_geschw(){

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return -1;
    }

    mysqlpp::Query query = conn.query();

    query << "SELECT AVG(velocity) AS avg_geschw FROM tmtrace WHERE id >= '" // Dieser Befehl liefert die Durchschnittlichegeschwindigkeit im Bereich der beiden IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;


    mysqlpp::StoreQueryResult res = query.store();

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return -1;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Durchschnitts Geschwindigkeit: ";
        cout << res[i]["avg_geschw"] << endl; // Ausgabe der Durchschnittsgeschwindigkeit.
    }
}


// max_schaden.
// Gibt die Maximalschadenspunktzahl aus.
// return: return -1 Wenn keine Datenbankverbindung aufgebaut werden konnte, return -1 Wenn keine Daten gefunden wurden.

int Ausgaben::max_schaden()
{
    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return -1;
    }

    mysqlpp::Query query = conn.query();

    query << "SELECT MAX(damage) AS schaden FROM tmtrace WHERE id >= '" // Dieser Befehl liefert den Maximalenschaden im Bereich der eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;

    mysqlpp::StoreQueryResult res = query.store();

    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return -1;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Maximaler Schaden: ";
        cout << res[i]["schaden"] << endl; // Ausgabe des Maximalen Schadens.
    }
}


// renndauer.
// Bestimmt die Dauer des Rennens.
// return: return -1 Wenn keine Datenbankverbindung aufgebaut werden konnte, return -1 Wenn keine Daten gefunden wurden.

int Ausgaben::renndauer()
{
    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return -1;
    }

    mysqlpp::Query query = conn.query();

    query << "select Max(racetime) - MAX(starttime) AS renndauer from tmtrace WHERE id >= '" // Dieser Befehl liefert die Dauer des Rennens im Bereich der eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;



    mysqlpp::StoreQueryResult res = query.store();

    float endzeit[30];
    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return -1;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Renndauer: ";
        cout << res[i]["renndauer"]; // Ausgabe der Renndauer.
    }
}


// rundenzahl.
// Gibt die Anzahl der absolvierten Runden aus.
// return: return -1 Wenn keine Datenbankverbindung aufgebaut werden konnte, return -1 Wenn keine Daten gefunden wurden.

int Ausgaben::rundenzahl(){

    mysqlpp::Connection conn(false);

    if(!conn.connect("rars","localhost",BENUTZER,PASSWORT)){

        cerr << "Connection failed" << endl;
        return -1;
    }

    mysqlpp::Query query = conn.query();

    query << "select Max(laps) AS rundenzahl from tmtrace WHERE id >= '" // Dieser Befehl liefert die Anzahl der Gefahrenen Runden im Bereich der eingegebenen IDs.
          << start_id << "' AND id <= '" << end_id << "' AND robot = '" << ROBOT << "';" << mysqlpp::quote;

    mysqlpp::StoreQueryResult res = query.store();

    float endzeit[30];
    if(!res){

        cerr << "Keine Daten gefunden" << endl;
        return -1;
    }

    for(size_t i = 0; i < res.num_rows(); i++){

        cout << "Gefahrene Runden: ";
        cout << res[i]["rundenzahl"]; // Ausgabe der Rundenzahl.
    }
}
