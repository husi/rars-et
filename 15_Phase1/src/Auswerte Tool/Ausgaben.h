
#ifndef AUSGABEN_H_INCLUDED
#define AUSGABEN_H_INCLUDED
#include <iostream>
#include <string>


using namespace std;

class Ausgaben{

    private:

    string start_id; // Hier wird die Start ID gespeichert z.B. 63241.
    string end_id; // Hier wird die End ID gespeichert z.B.63242.


    public:

    Ausgaben();

    int renn_daten(); // Gibt die Daten aller gespeicherten Rennen auf dem Bildschirm aus (Datum, Streckenname, Start ID und End ID).

    int max_id(); // Gibt die letzte ID Adresse aus.

    void daten_eingabe(); // Eingabe der Start und End ID.

    void daten_anzeige(); // Gibt die momentan gespeicherte Start und End ID aus.

    int max_geschw(); // Gibt die Maximalgeschwindigkeit aus.

    int avg_geschw(); // Bestimmt die Durchschnittsgeschwindigkeit.

    int max_schaden(); // Gibt die Maximalschadenspunktzahl aus.

    int renndauer(); // Bestimmt die Dauer des Rennens.

    int rundenzahl(); // Gibt die Anzahl der absolvierten Runden aus.


};


#endif // ANLAGE_H
