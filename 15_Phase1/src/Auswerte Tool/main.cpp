
#include <mysql++.h>
#include <iostream>
#include <stdio.h>
#include <string.h>

#include "Ausgaben.h"

using namespace std;

// enter
// Sorgt dafür dass immer nur der aktuelle Teil des Programms angezeigt wird und ältere Programmausgaben im Konsolefenster gellöscht werden.

void enter(){ // wartet auf eingabe der enter Taste.
    // Löscht etwaige Fehlerzustände die das Einlesen verhindern könnten.
    cin.clear();
    // Ignoriert soviele Zeichen im Puffer wie im Puffer vorhanden sind.
    cin.ignore(numeric_limits<streamsize>::max(), '\n');
    // Füge alle eingelesenen Zeichen in den Puffer bis ein Enter gedrückt wird.
    // cin.get() liefert dann das erste Zeichen aus dem Puffer zurück, welches aber ignoriert wird (interessiert ja nicht).
    cin.get();
}





int main(){

    Ausgaben aus;



    bool ende; // Variable um die Schleife zu beenden.

    ende = true;

    do{

        int wahl; // Variable für die Hauptmenue Auswahl.
        system("clear"); // löscht die gesamte display Anzeige.
        cout << "Hauptmenue\n" << endl // Anzeige Hauptmenue.
             << "1. Daten von allen Rennen anzeigen: " << endl
             << "2. Daten ändern: " << endl
             << "3. Daten anzeigen: " << endl
             << "4. Maximale Geschwingidkeit: " << endl
             << "5. Durchschnitts Geschwindigkeit: " << endl
             << "6. Maximaler Schaden: " << endl
             << "7. Renndauer: " << endl
             << "8. Rundenanzahl: " << endl
             << "0. Ende" << endl;

        cin >> wahl; // Hauptmenue Auswahl.



        switch(wahl){

            case 1:
            system("clear"); // sorgt dafür dass nur der ausgewählte Menuepunkt in der Console angezeigt wird.
            aus.renn_daten();
            aus.max_id();
            enter(); // wartet auf eingabe der enter Taste.
            break;

            case 2:
            system("clear");
            aus.daten_eingabe();
            enter();
            break;

            case 3:
            system("clear");
            aus.daten_anzeige();
            enter();
            break;

            case 4:
            system("clear");
            aus.max_geschw();
            enter();
            break;

            case 5:
            system("clear");
            aus.avg_geschw();
            enter();
            break;

            case 6:
            system("clear");
            aus.max_schaden();
            enter();
            break;

            case 7:
            system("clear");
            aus.renndauer();
            enter();
            break;

            case 8:
            system("clear");
            aus.rundenzahl();
            enter();
            break;

            case 0:
            cout << "Beende Programm" << endl;
            ende = false; // Programm wird beendet schleifenbedingung wird nicht mehr erfüllt.
            break;

            default: // wird bei falscher Eingabe Aufgerufen.
            system("clear");
            cout << "Falsche eingabe" << endl;
            enter();
        }

    } while(ende);

    return 0;
}
